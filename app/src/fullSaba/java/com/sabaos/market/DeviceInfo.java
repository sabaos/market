package com.sabaos.market;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class DeviceInfo {

    private Context context;

    public DeviceInfo(Context context) {

        this.context = context;
    }

    public String getApplicationVersion() {

        String appVersion = "notFound";
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo info = packageManager.getPackageInfo(context.getPackageName(), 0);
            appVersion = info.versionName;
        } catch (Exception e) {

        }
        return appVersion;
    }

    public String getAndroidId() {

        String androidId =  "notFound";
        try {
            androidId = Settings.System.getString(context.getContentResolver(), Settings.System.ANDROID_ID);
        } catch (Exception e) {

        }

        return androidId;
    }

    public String getIMEI() {

        ArrayList<String> IMEI = new ArrayList<String>();
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {

            if (Build.VERSION.SDK_INT >= 26) {

                int phoneSlots = tm.getPhoneCount();

                for (int i = 0; i <= phoneSlots; i++) {

                    IMEI.add(tm.getImei(i));
                }
            } else {

                int phoneSlots = tm.getPhoneCount();

                for (int i = 0; i <= phoneSlots; i++) {

                    IMEI.add(tm.getDeviceId(i));
                }
            }
        }

        String phoneIMEI = "";
        for (int i = 0; i < IMEI.size(); i++) {

            phoneIMEI += "&imei[" + String.valueOf(i) + "]" + "=" + IMEI.get(i);

        }

        return phoneIMEI;
    }

    public String getfirstIMEI() {

        String phoneIMEI = "";
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {

            if (Build.VERSION.SDK_INT >= 26) {

                phoneIMEI = tm.getImei();

            } else {

                phoneIMEI = tm.getDeviceId();
            }
        }
        return phoneIMEI;
    }

    public String getPhoneSerialNumber() {

        String phoneSerialNumber = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);

            phoneSerialNumber = (String) get.invoke(c, "sys.serialnumber", "error");
            if (phoneSerialNumber.equals("error")) {
                phoneSerialNumber = (String) get.invoke(c, "ril.serialnumber", "error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return phoneSerialNumber;
    }

    public String getHWSerialNumber() {

        String hwSerialNumber = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            hwSerialNumber = Build.getSerial(); // Requires permission READ_PHONE_STATE
        } else {
            hwSerialNumber = Build.SERIAL; // Will return 'unknown' for device >= Build.VERSION_CODES.O
        }
        return hwSerialNumber;
    }

    public String getOsName() {

        return Build.VERSION.RELEASE;
    }

    public String getPhoneModel() {

        return Build.MANUFACTURER
                + "_" + Build.MODEL;
    }

    public String getOsSecurityLevel() {

        String LEVEL_KEY = "ro.build.seclevel";
        return getSystemProperty(context, LEVEL_KEY);
    }

    static String getSystemProperty(Context context, String key) {
        // Using reflection to access hidden SystemProperties class in android API
        //   based on https://stackoverflow.com/q/2641111
        String level;
        try {
            ClassLoader cl = context.getClassLoader();
            Class SystemProperties = cl.loadClass("android.os.SystemProperties");
            Class[] paramTypes = new Class[1];
            paramTypes[0] = String.class;
            Method get = SystemProperties.getMethod("get", paramTypes);
            Object[] params = {key};
            level = (String) get.invoke(SystemProperties, params);
        } catch (Exception e) {
            level = "generic";
        }
        if (level.equalsIgnoreCase("")) level = "generic";
        return level;
    }
}
