package com.sabaos.market;

import android.graphics.drawable.Drawable;


// This class defines the class from which its instances are passed to SecurityEventsAdapter

public class SecurityEvent {

    private String type;
    private String packageName;
    private String appName;
    private Drawable icon;

    public SecurityEvent(String type, String packageName, String appName, Drawable icon) {
        this.type = type;
        this.packageName = packageName;
        this.appName = appName;
        this.icon = icon;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getAppName() {
        return appName;
    }

    public Drawable getIcon() {
        return icon;
    }
}
