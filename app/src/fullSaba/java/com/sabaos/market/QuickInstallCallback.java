package com.sabaos.market;

// Used to register callback upon QuickInstall

public class QuickInstallCallback {

    QuickInstallCallbackListener quickInstallCallbackListener;

    public void registerCallback(QuickInstallCallbackListener quickInstallCallbackListener) {
        this.quickInstallCallbackListener = quickInstallCallbackListener;
    }

    public void callback() {
        quickInstallCallbackListener.onCallbackListener();
    }
}
