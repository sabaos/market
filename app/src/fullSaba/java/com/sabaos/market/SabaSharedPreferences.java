package com.sabaos.market;

import android.content.Context;

import java.util.Map;


// Saves installed shortcut
// Since there no way to find out if the user has uninstalled a shortcut when the Market is not running,
// We save installed shortcuts here. When Market is started, it checks installed shortcuts taken from
// Android against its saved values. If there is a saved value that does not correspond to an installed
// shortcut, it means that the user has deleted the shortcut when app was not running, thus we remove
// the shortcut from saved values. This allows the shortcut app to be available again for download in the Market.

public class SabaSharedPreferences {

    private Context context;
    private android.content.SharedPreferences sharedPreferences;
    private android.content.SharedPreferences.Editor editor;

    public SabaSharedPreferences(Context context) {
        this.context = context;
        sharedPreferences = (android.content.SharedPreferences) context.getSharedPreferences("webAppIcons", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void saveData(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getData(String key) {
        return sharedPreferences.getString(key, null);
    }

    public Map getAll() {
        return sharedPreferences.getAll();
    }

    public void removeData(String key) {
        editor.remove(key);
        editor.commit();
    }

}
