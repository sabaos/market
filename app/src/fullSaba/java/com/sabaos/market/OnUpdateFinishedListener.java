package com.sabaos.market;

// Callback interface to notify Market is refreshed

public interface OnUpdateFinishedListener {

    void onUpdateFinished();
}
