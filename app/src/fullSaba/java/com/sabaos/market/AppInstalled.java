package com.sabaos.market;

// Used to register callback upon Apk install

public class AppInstalled {

    private OnAppInstalledListener onAppInstalledListener;

    public void registerListener(OnAppInstalledListener onAppInstalledListener) {
        this.onAppInstalledListener = onAppInstalledListener;
    }

    public void listenToInstalls() {
        onAppInstalledListener.onInstalled();
    }
}
