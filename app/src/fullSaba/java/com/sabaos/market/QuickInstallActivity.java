package com.sabaos.market;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.fdroid.fdroid.R;
import org.fdroid.fdroid.data.Apk;
import org.fdroid.fdroid.data.ApkProvider;
import org.fdroid.fdroid.data.App;
import org.fdroid.fdroid.views.AppDetailsActivity;

import java.util.ArrayList;

import static org.fdroid.fdroid.FDroidApp.globalContext;
import static org.fdroid.fdroid.views.whatsnew.WhatsNewAdapter.appArrayList;


// This activity helps users install several apps together
// It chooses some hardcoded apps from cursor received from server and displays them to user.


public class QuickInstallActivity extends AppCompatActivity implements OnApkDownloadedListener,
        OnAppInstalledListener {
    public RecyclerView quickInstallRecyclerView;
    Button button;
    private int numberOfAppsToBeInstalled = 0;
    private int counter = -1;
    public static Apk installApk;
    QuickInstallAdapter quickInstallAdapter;
    private ArrayList<App> appsToInstall = new ArrayList<>();
    public static String mutex = "";
    private AppDetailsActivity appDetailsActivity;
    private ProgressBar installProgressbar;
    private int progressBarMaxValue;
    private TextView progressBarTextView;
    private TextView progressBarTitle;
    private LinearLayout progressBarLayout;
    private boolean canExitActivity;
    private String appName;
    private AppCompatActivity activity;
    public static String installMode = "";
    public static App appToInstall;
    public static ArrayList<AppInstallationReport> appInstallationReports = new ArrayList<>();
    private boolean isAlreadyRun = false;
    private boolean quickInstallReportRunning = false;
    private static boolean quickInstall = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_install);
//        isAlreadyRun = false;
        activity = this;
        canExitActivity = true;
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.quick_install_actionbar);
        setTitle(getResources().getString(R.string.quick_install_activity_name));
        progressBarTitle = findViewById(R.id.progressbar_title);
        quickInstallAdapter = new QuickInstallAdapter("", 100, appArrayList);
        installProgressbar = (ProgressBar) findViewById(R.id.install_progressbar);
        progressBarTextView = findViewById(R.id.progress_bar_text);
        progressBarLayout = findViewById(R.id.progress_bar_layout);
        quickInstallRecyclerView = findViewById(R.id.app_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        quickInstallRecyclerView.setLayoutManager(linearLayoutManager);
        quickInstallRecyclerView.setAdapter(quickInstallAdapter);
        quickInstallRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        button = findViewById(R.id.quick_install);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appsToInstall.clear();
                quickInstall = true;
                counter = -1;
                appsToInstall = quickInstallAdapter.getAppsToInstall();
                numberOfAppsToBeInstalled = appsToInstall.size();
                progressBarMaxValue = numberOfAppsToBeInstalled;
                installProgressbar.setMax(progressBarMaxValue);
                appInstallationReports.clear();
                if (numberOfAppsToBeInstalled > 0) {
                    for (App app : appsToInstall) {
                        appInstallationReports.add(new AppInstallationReport(app, false, 100));
                    }
                    canExitActivity = false;
                    button.setClickable(false);
                    button.setVisibility(View.GONE);
                    setProgressBarValue(0);
                    quickInstallRecyclerView.setVisibility(View.GONE);
                    progressBarLayout.setVisibility(View.VISIBLE);
                    installProgressbar.setVisibility(View.VISIBLE);
                    progressBarTextView.setVisibility(View.VISIBLE);
                    progressBarTitle.setVisibility(View.VISIBLE);
                    installApp();
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(globalContext, getString(R.string.no_app_selected), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }


    // Runs when an Apk is downloaded

    @Override
    public void onDownloaded() {

    }

    // Sets progressbar new value

    private void setProgressBarValue(final int progress) {
        final int value;
        value = progress + 1;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                installProgressbar.setProgress(value);
                StringBuilder stringBuilder = new StringBuilder();
                int progress = (int) Math.round(value / Double.valueOf(progressBarMaxValue) * 100);
                if (progress > 100) progress = 100;
                stringBuilder.append(String.valueOf(progress));
                stringBuilder.append("%");
                stringBuilder.append(" نصب شده");
                progressBarTextView.setText(stringBuilder.toString());
            }
        });
    }

    // Installs chosen apps

    public void installApp() {


        try {
            if (installProgressbar != null)
                setProgressBarValue(counter);
            counter += 1;
            if (counter < appsToInstall.size()) {
                App app = appsToInstall.get(counter);
                appToInstall = app;
                appName = app.name;
                Log.v("QIRRR", "install app called for :" + appName);
                appDetailsActivity = new AppDetailsActivity(app, "quickInstall", globalContext, activity, new OnQuickInstallAppStatus() {
                    @Override
                    public void onDownload() {
                        String downloading = globalContext.getString(R.string.downloading) + " " + appName;
                        progressBarTitle.setText(downloading);
                        Log.v("QIRRR", "downloading :" + appName);
                    }

                    @Override
                    public void onDownloadError() {
                        if (activity != null) {
                            String downloadError = String.format(globalContext.getString(R.string.download_interrupted), appName);
                            progressBarTitle.setText(downloadError);
                            for (AppInstallationReport appInstallationReport : appInstallationReports) {
                                if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name) && !appInstallationReport.isInstalled()) {
                                    Log.v("QIRRR", "download error :" + appName);
                                    appInstallationReport.setInstalled(false);
                                    appInstallationReport.setErrorCode(3);
                                    break;
                                }
                            }
                            try {
//                                checkWebAppInstallations();
                                if (anyAppInstalled()) {
                                    QuickInstallPreferences quickInstallPreferences = new QuickInstallPreferences(globalContext);
                                    quickInstallPreferences.saveData("quickInstall", "true");
                                }
                                canExitActivity = true;
                                installMode = "";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isAlreadyRun) {
                                            loadInstallReport();
                                        }
                                        isAlreadyRun = true;
                                    }
                                }, 1000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onInstall() {
                        String installing = globalContext.getString(R.string.installing) + " " + appName;
                        progressBarTitle.setText(installing);
                        Log.v("QIRRR", "installing :" + appName);
                    }

                    @Override
                    public void onInstallError() {
                        String installInterrupted = String.format(globalContext.getString(R.string.install_error), appName);
                        progressBarTitle.setText(installInterrupted);
                        for (AppInstallationReport appInstallationReport : appInstallationReports) {
                            if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name) && !appInstallationReport.isInstalled()) {
                                Log.v("QIRRR", "installing :" + appName);
                                appInstallationReport.setInstalled(false);
                                appInstallationReport.setErrorCode(2);
                                onInstalled();
                                break;
                            }
                        }
                    }

                    @Override
                    public void onInstallComplete() {
                        String installInterrupted = String.format(globalContext.getString(R.string.install_complete), appName);
                        progressBarTitle.setText(installInterrupted);
                        for (AppInstallationReport appInstallationReport : appInstallationReports) {
                            if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name) && !appInstallationReport.isInstalled()) {
                                Log.v("QIRRR", "installed :" + appName);
                                appInstallationReport.setInstalled(false);
                                appInstallationReport.setErrorCode(0);
                                onInstalled();
                                break;
                            }
                        }
                    }
                });
                Apk apkToInstall = ApkProvider.Helper.findSuggestedApk(globalContext, app);

                //if Market did not find any relevant Apk to install, move on to install the next app
                if (apkToInstall == null) {
                    installApp();
                }
                installApk = apkToInstall;
                appDetailsActivity.onCreate(null, null);
                appDetailsActivity.installApk(apkToInstall);


            } else {
                progressBarTitle.setText(globalContext.getString(R.string.app_install_finished));
                if (installProgressbar != null) setProgressBarValue(progressBarMaxValue);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anyAppInstalled()) {
                                QuickInstallPreferences quickInstallPreferences = new QuickInstallPreferences(globalContext);
                                quickInstallPreferences.saveData("quickInstall", "true");
                            }
                            canExitActivity = true;
                            installMode = "";
                            loadInstallReport();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(globalContext, globalContext.getString(R.string.unfortuantely) + " " + appName + globalContext.getString(R.string.not_installed), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    // Runs when Apk is installed

    @Override
    public void onInstalled() {

        if (quickInstall) {
            Log.v("QIRRR", "onInstalled() called for :" + appName);

            // if Apk did not install because of reason, display it as a Toast
            installApp();
        }
    }


    // if the user has chosen some apps to install and has pressed the install button, disable the
    // back button, because we need him to stay on this activity. if not, he can quit. if the user
    // closes the activity and reopens it, he doesn't see the installed apps anymore. He just seen the
    // remaining app from the list which he can choose to install.

    @Override
    public void onBackPressed() {
        if (canExitActivity) {
            super.onBackPressed();
        }
    }

    private void loadInstallReport() {

        if (!quickInstallReportRunning) {
            Intent intent = new Intent(globalContext, QuickInstallReport.class);
            globalContext.startActivity(intent);
            if (activity != null) activity.finish();
            quickInstallReportRunning = true;
        }
    }

    private boolean anyAppInstalled() {
        boolean anyAppInstalled = false;
        for (AppInstallationReport appInstallationReport : appInstallationReports) {
            if (appInstallationReport.isInstalled()) {
                anyAppInstalled = true;
                break;
            }
        }
        return anyAppInstalled;
    }
}