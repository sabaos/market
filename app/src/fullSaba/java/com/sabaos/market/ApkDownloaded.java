package com.sabaos.market;

import org.fdroid.fdroid.data.Apk;
import org.fdroid.fdroid.data.App;

// Used to register callback upon Apk download

public class ApkDownloaded {

    private OnApkDownloadedListener apkDownloadedListener1;

    public void registerOnApkDownloadedListener(OnApkDownloadedListener apkDownloadedListener) {
        apkDownloadedListener1 = apkDownloadedListener;
    }

    public void installSabaApps(Apk apkToInstall, App appToInstall) {
        apkDownloadedListener1.onDownloaded();
    }
}
