package com.sabaos.market;


// listener for tracking procedure of application download and install

public interface OnQuickInstallAppStatus {

    void onDownload();
    void onDownloadError();
    void onInstall();
    void onInstallError();
    void onInstallComplete();
}
