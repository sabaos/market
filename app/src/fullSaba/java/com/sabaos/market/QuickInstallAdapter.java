package com.sabaos.market;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.fdroid.fdroid.R;
import org.fdroid.fdroid.data.App;

import java.util.ArrayList;

import static org.fdroid.fdroid.FDroidApp.globalContext;

// This Adapter chooses some apps from the Cursor received from server and display them to the user

public class QuickInstallAdapter extends RecyclerView.Adapter<QuickInstallAdapter.AppsToInstallViewHolder> {

    boolean[] appsToInstallChecker;
    ArrayList<App> appArrayList = new ArrayList<>();
    String type;
    int i;

    public QuickInstallAdapter(String type, int i, ArrayList<App> appArrayList1) {
        super();
        for (App app : appArrayList1) {
            switch (app.packageName) {

                case "ir.mci.ecareapp":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                case "net.cozic.joplin":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                case "com.github.axet.bookreader":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                case "ir.balad":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                case "com.byagowi.persiancalendar":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                case "com.quran.labs.androidquran":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                case "ir.nasim":
                    if (globalContext.getPackageManager().getLaunchIntentForPackage(app.packageName) == null) {
                        this.appArrayList.add(app);
                    }
                    break;

                default:
                    break;

            }
        }
        appsToInstallChecker = new boolean[appArrayList.size()];
        for (int j = 0; j < appsToInstallChecker.length; j++) {
            appsToInstallChecker[j] = true;
        }
        this.type = type;
        this.i = i;
    }

    @NonNull
    @Override
    public QuickInstallAdapter.AppsToInstallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_layout, parent, false);
        return new AppsToInstallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuickInstallAdapter.AppsToInstallViewHolder holder, final int position) {

        App app = appArrayList.get(position);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    appsToInstallChecker[position] = true;
                } else {
                    appsToInstallChecker[position] = false;
                }
            }
        });
        holder.textView.setText(app.name);

    }

    @Override
    public int getItemCount() {
        return appArrayList.size();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class AppsToInstallViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        TextView textView;

        public AppsToInstallViewHolder(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            textView = itemView.findViewById(R.id.title);
        }
    }

    public ArrayList<App> getAppsToInstall() {
        ArrayList<App> appsToInstall = new ArrayList<>();
        for (int i = 0; i < appsToInstallChecker.length; i++) {
            if (appsToInstallChecker[i] == true) {
                appsToInstall.add(appArrayList.get(i));
            }
        }
        return appsToInstall;
    }
}
