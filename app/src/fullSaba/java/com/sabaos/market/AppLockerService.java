package com.sabaos.market;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import org.fdroid.fdroid.R;
import org.fdroid.fdroid.views.main.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import static com.sabaos.market.InstalledAppExaminer.nonSabaAppsPackageNames;
import static org.fdroid.fdroid.views.main.MainActivity.mainActivity;

public class AppLockerService extends Service {

    public static final String CHANNEL_ID = "Saba Market Service";
    private Context context;
    public static String lockedApp;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = getApplicationContext();
        Intent intent1 = new Intent(context, MainActivity.class);
        intent1.putExtra("source", "security_notif");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= 26) {
            createNotificationChannel();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel(context.getString(R.string.saba_market), context.getString(R.string.saba_market), NotificationManager.IMPORTANCE_LOW);
            notificationManager.createNotificationChannel(notificationChannel);
            Notification.Builder notification = new Notification.Builder(context, context.getString(R.string.saba_market))
                    .setSmallIcon(R.drawable.ic_saba_market)
                    .setContentTitle(context.getString(R.string.saba_market))
                    .setContentText(context.getString(R.string.security_notif_description))
                    .setContentIntent(pendingIntent);
            startForeground(12, notification.build());
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        isConcernedAppIsInForeground(context, nonSabaAppsPackageNames);
                    }
                }, 0, 500);
            }
        });
        thread.run();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Saba Market Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );
            NotificationManager manager = (NotificationManager) getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public boolean isConcernedAppIsInForeground(final Context context, ArrayList<String> packageNames) {

        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> task = manager.getRunningTasks(200);
        if (Build.VERSION.SDK_INT <= 20) {
            if (task.size() > 0) {
                ComponentName componentInfo = task.get(0).topActivity;
                // Check component against all installed apps
            }
        } else {
            UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 500, time);
            if (stats != null) {
                SortedMap<Long, UsageStats> runningTask = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : stats) {
                    if (usageStats.getLastTimeUsed() >= time - 1000) {
                        runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                    }
                }
                if (runningTask.isEmpty()) {

                } else {
                    String runningApp = runningTask.get(runningTask.lastKey()).getPackageName();
                    Log.v("running app: ", runningApp);
                    for (String packageName : packageNames) {
                        Log.v("testing for ", packageName);
                        if (packageName.equalsIgnoreCase(runningApp)) {
                            int a = runningTask.get(runningTask.lastKey()).hashCode();
                            Log.v("detected1 " + packageName, String.valueOf(a));
                            lockedApp = packageName;
                            context.startActivity(new Intent(context, LockScreenActivity.class));
                            try {
                                mainActivity.finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}