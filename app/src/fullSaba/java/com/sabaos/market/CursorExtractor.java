package com.sabaos.market;

import android.database.Cursor;

import org.fdroid.fdroid.data.App;

import java.util.ArrayList;

// Converts Cursor received from Market server that contains all apps with their info to
// an ArrayList of apps with their info

public class CursorExtractor {

    public ArrayList<App> extractAppsFromCursor(Cursor cursor) {

        ArrayList<App> appArrayList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                appArrayList.add(new App(cursor));
            } while (cursor.moveToNext());
        }

        return appArrayList;
    }
}
