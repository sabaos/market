package com.sabaos.market;

import java.util.ArrayList;

public interface OnTaskExecution {
    void onTaskFinished(ArrayList<SecurityEvent> securityEvents);
}
